'use strict';

describe('Directive: jsoneditor', function () {

  // load the directive's module
  beforeEach(module('jsoneditordemoApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<jsoneditor></jsoneditor>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the jsoneditor directive');
  }));
});
