'use strict';
angular.module('myApp', [
	'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
   	'ngSanitize',
	  'ngTouch',
  	'ui.bootstrap',
  	'jsoneditorDirective'
]).config(function ($routeProvider) {
    $routeProvider
      .when('/', {
      	templateUrl: 'view.html',
        controller: 'AppCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });