'use strict';

/**
 * @ngdoc function
 * @name jsoneditordemoApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the jsoneditordemoApp
 */
angular.module('jsoneditordemoApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
