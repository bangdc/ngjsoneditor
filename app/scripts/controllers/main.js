'use strict';

/**
 * @ngdoc function
 * @name jsoneditordemoApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the jsoneditordemoApp
 */
angular.module('jsoneditordemoApp')
  .controller('MainCtrl', function ($scope) {
    var $nullArray  = [];
    var $nullObject = {};
  	var $initValue 	=	{
					  		'array': [
							    1,
							    2,
							    3
						  	],
						  	'boolean': true,
						  	'null': null,
						  	'number': 123,
						  	'object': {
						    	'a': 'b',
						    	'c': 'd',
						    	'e': 'f'
						  	},
						  	'string': 'Hello World'
						};
	var $customer 	=	{
					     	"firstName": "John",
					     	"lastName": "Smith",
					     	"age": 25,
					     	"address":
					     	{
					         	"streetAddress": "21 2nd Street",
					         	"city": "New York",
					         	"state": "NY",
					         	"postalCode": "10021"
					     	},
					     	"phoneNumber":
					     	[
						         {
						           "type": "home",
						           "number": "212 555-1234"
						         },
						         {
						           "type": "fax",
						           "number": "646 555-4567"
						         }
						     ]
						 };
	var $employees = [
	    {"firstName":"John", "lastName":"Doe"}, 
	    {"firstName":"Anna", "lastName":"Smith"}, 
	    {"firstName":"Peter", "lastName": "Jones"}
	];
    $scope.test   	= 	JSON.stringify($initValue);
    $scope.test1 	=	JSON.stringify($customer);
    $scope.test2	=	JSON.stringify($employees);
    $scope.test3    =   "Hello jsoneditor, the beautiful directive. But this is not a correct json format.";
    $scope.submitConfig 	=	{
    	'data'	: 	{},
    	'model'	: 	'json_name',	//Name of data
    	'method' 	:  	'POST', 	//POST,
    	'url'	: 	'http://admintable.api/jsontest.php',
    };
    var $a  =   []; //array
    var $b  =   {}; //object
    $scope.formTest 	=	{
    	userName: '',
    	password: '',
    	schools: JSON.stringify($a),
        extraData: JSON.stringify($b),
    };
    $scope.submitForm 	=	function(){
   		alert('You have tried the directive ng-jsoneditor, is it awesome and useful? Go buy it now. :) Thank you so much !');
    };
  });
