'use strict';

/**
 * @ngdoc overview
 * @name jsoneditordemoApp
 * @description
 * # jsoneditordemoApp
 *
 * Main module of the application.
 */
angular
  .module('jsoneditordemoApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'jsoneditorDirective',
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
